# Préparation du Turtlebot sous ROS1 Noetic

Ce répertoire contient les étapes de la mise en place du système d'exploitation pour les turtlebots.

## Étapes
1. OS :
 1. Télécharger l'image du système d'exploitation avec ros noetic à l'intérieur depuis <a href="https://ubiquity-pi-image.sfo2.digitaloceanspaces.com/2021-08-06-focal.img.xz">Ubiquity</a>
 2. Ou téléchargez le serveur ubuntu 20.04 LTS et configurez-le avec noetic.
2. Modifier la langue : `sudo raspi-config` et changer l'emplacement.
3. Connecter le raspberry à l'internet puis sous `/etc/netplan`, créez `50-cloud-init.yaml` et modifiez-le comme suit:
```yaml
network:
    wifis:
        wlan0:
            dhcp4: yes
            dhcp6: yes
            access-points:
                ssid_:
                    password: password_
            optional: yes 
```  
4. Assurez-vous que le wifi est activé :
```console
sudo ip link set wlan0 up
```

5. Connectez et configurez les changements :
```console
sudo netplan —debug try
sudo netplan —debug generate
sudo netplan —debug apply
```

6. Vérifiez l'adresse IP 
```console
hostname -I
```

7. L'exporter vers le maître et l'hôte
```console
export ROS_MASTER_URI=http://ip_address_tb3:11311
export ROS_HOSTNAME=ip_address_tb3
```

8. Flachez le OpenCR en installant le program depuis <a href="https://emanual.robotis.com/docs/en/platform/turtlebot3/opencr_setup/#opencr-setup">Emanual.Robotis</a>, vérifiez le modèle du robot utilisé (`burger`, `waffle`) puis exécutez:
```console
./update.sh $PORT $MODEL.opencr
```
  - `$MODEL` = burger_noetic, waffle_noetic, burger, waffle
  - `$PORT` = `/dev/ttyACM0` d'habitude, mais vérifiez le en comparant par la commande `ls /dev/ttyACM*`

9. Copier le repo de turtlebot3 depuis github dans le lien vers le répertoire `catkin_ws/src/`
```console
git clone https://github.com/ROBOTIS-GIT/turtlebot3
```

10. Dans le fichier `catkin_ws/`, installez les dépendances :
```console
rosdep install —from-paths src —ignore-src –r –y
```

11. Construire le fichier `catkin_ws/`
```console
catkin_make
```

12. Sourcez le package:
```console
source devel/setup.bash
```

13. Exécutez le code :
```console
roslaunch turtlebot3_bringup turtlebot3_robot.launch
```




 
