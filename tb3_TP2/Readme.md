# TP manipulation d'un robot réel
### Création de la liaison entre le pc et le turtlebot (dans un nouveau terminal)

_Pour la préparation du système opérationnel du turtlebot, suivez <a href="SetupTurtlebot.md">SetupTurtlebot.md</a>_

Pour pouvoir superviser le turtlebot et le piloter à distance, nous avons besoin de créer une liaison vers le PC.

Cela permettra de créer une relation maître/esclave. Pour cela nous avons besoin de connaître les adresses IP du pc et du turtlebot afin de créer une communication entre les deux. Initialement, il faut relier le robot à un écran pour trouver son adresse IP. Pour connaître cette information, on utilise la commande suivante pour le turtlebot et le PC:

```console
hostname -I
```

A partir de maintenant, nous appellerons `IP_OF_TURTLEBOT` l'adresse IP du turtlebot et `IP_OF_PC`, l'adresse IP de la PC. Lorsque `IP_OF_TURTLEBOT` ou `IP_OF_PC` apparaîtront dans les commandes ci-dessous, il suffira de les remplacer par les adresses IP déterminées précédemment afin que les commandes s'exécutent.

> :warning: Il faut s'assurer que les deux adresses IP appartiennent à la même classe, et par suite au même réseau.

Après avoir eu `IP_OF_TURTLEBOT`, on aura plus besoin de connecter le robot à un écran. Il suffit donc de le remplacer par un terminal via une connexion SSH depuis le PC par la commande :  

```console
ssh ubuntu@IP_OF_TURTLEBOT
```

puis le mot de passe du turtlebot.

<strong>Sur le terminal du turtlebot :</strong>
```console
export ROS_MASTER_URI=http://IP_OF_PC:11311
```

Permet de créer une connexion Web. C'est cette connexion qui servira de passerelle pour relier le pc au turtlebot.

> :warning: ATTENTION!! Le port 11311 représente le port de connexion. Il s'agit d'une adresse 16 bits, et peut contenir des valeurs comprises entre 0 et 65 535. Il faut surtout veiller à ne pas dépasser les 5 chiffres. car dans ce cas il faudra aller modifier le code directement dans le fichier caché `.bashrc`. 

> :warning:
>  Il faut définir les variables d'environnement ROS_MASTER_URI et ROS_HOSTNAME sur chaque terminal, ou les définir une fois pour toutes sur le fichier caché `.bashrc`. Il faut aussi re-sourcer chaque terminal qui a été ouvert avant le changement de la configuration du `.bashrc`.

>```console
>source ~/.bashrc
>```

Pour nommer le turtlebot par son adresse IP:
```console
export ROS_HOSTNAME=IP_OF_TURTLEBOT
```

<strong>Sur le terminal du PC :</strong>
```console
export ROS_MASTER_URI=http://IP_OF_PC:11311
```
Pour connecter le PC à la passerelle.
```console
export ROS_HOSTNAME=IP_OF_PC
```
Permet de nommer le PC par son adresse IP.
A partir de maintenant, le PC est opérationnel.

> :warning:
> VERIFICATION
> Les instructions suivantes permettent de vérifier que les commandes exécutées précédemment sont les bonnes et ont bien fonctionné.
> sur le turtlebot
>```
>echo $ROS_MASTER_URI
>```
> Cette commande doit retourner l'adresse de la passerelle :
>```
>http://IP_OF_PC:11311
>```

>```console
>echo $ROS_HOSTNAME
>``` 
> Cette commande doit retourner le nom associé au turtlebot (son adresse IP ici) :
>```
>IP_OF_TURTLEBOT
>```
>Sur le PC :
> ```console
>echo $ROS_MASTER_URI
>```
> Cette commande doit retourner l'adresse de la passerelle :
> ```
>http://IP_OF_PC:11311
>```

>```console
>echo $ROS_HOSTNAME
>``` 
> Cette commande doit retourner le nom associé au turtlebot (son adresse IP ici) :
> ```console
>IP_OF_PC
>```

> :warning:
> la communication établie suit une architecture Maître/Esclave : Le Maître utilise son adresse IP pour les deux variables ROS_MASTER_URI et ROS_HOSTNAME. Il n'a pas besoin de connaître les adresses IP de ses esclaves. Tandis qu`un esclave doit connaître l`adresse IP du Maître, pour qu`il sache à qui il doit rapporter les données générées.


## Démarrer le robot réel :
Sur PC, actant comme Maître, lancer ros sur la machine:
```console
roscore
```

Dans le turtlebot ou sur terminal de la connexion ssh au turtlebot, tapez:
```console
roslaunch turtlebot3_bringup turtlebot3_robot.launch
```
- `roslaunch` permet d`exécuter un fichier .launch
- `turtlebot3_bringup`: dossier dans lequel le fichier `turtlebot3_robot.launch` est situé.
- Emplacement du ficher : `/home/utilisateur/catkin_ws/src/turtlebot3_bringup/launch`

> :warning:
> Eviter le copier/coller (`ctrl+C` / `ctrl+V`) bien connu sous Windows. Sous Linux, il se réalise avec sélection du texte par clic gauche de la souris, puis clic milieu pour copier (ou `ctrl+shift+C`/`ctrl+shift+V`). Pour la raison détaillée ci-dessus, il est préférable d'éviter les `ctrl+C` sous peine de fermer involontairement un processus en cours.

A partir de maintenant, nous présentons les différentes commandes permettant de contrôler le turtlebot.

Dans un premier temps, nous allons développer chaque commande individuellement, puis nous verrons comment les lancer ensemble (en même temps).

##  Lecture des données Turtlebot3
- `rosnode list` : donne la liste des packet disponibles (/nom_du_node1 /nom_du_node2 …).
- `rostopic list` : donne la liste des topics disponibles (/nom_du_topic1 /nom_du_topic2 …).
- `rosnode info /nom_du_node` : pour savoir les subscribers/publishers/services et connections
- `rostopic info /nom_du_topic` : pour avoir plus d`informations sur le type du message passant par un certain topic.
- `rosmsg show type_of_msg` : donne le format et le contenu du message.

Les Types de messages répandues sont :
*	Pour le topic `/cmd vel` (Command Velocity): c'est de type geometry msgs/Twist.
*	Pour le topic `/pose`: c'est le type geometry msgs/pose.
*	Pour le topic `/odometry`: c'est le type nav msgs/Odometry.

- `rostopic echo /nom_du_topic name`: donne le contenu du message courant.
- `rostopic pub -1 /nom_du_topic type_of_msg – ’[ ]’ ’[ ]’ ..` : imposer et publier un message sur un topic.
- `rosrun rqt graph rqt graph`: pour voir la connexion des nœuds et les topics actives dans la configuration ROS courante.
- `rostopic hz /nom_du_topic` : pour savoir la fréquence d'un topic.

##  Enregistrement des données Turtlebot3

Le ROSBag est un outil puissant pour enregistrer et refaire la simulation, selon les topics sélectionnées, la fréquence de lecture et sa durée. Elle possède une très grande vérité de commandes, voici les plus essentiels:
-  `rosbag record -o name.bag /nom_du_topic1 /nom_du_topic2` : enregistrement des valeurs de topic mentionnées dans le fichier name.bag
-  `rosbag record --duration=t /nom_du_topic` : détermination de la durée d’enregistrement ”t” des topics mentionnés.
-  `rosbag play -r freq name.bag`: relire les données enregistrées dans le fichier name.bag avec une fréquence freq fois plus rapide.
-  `rqt_bag` : permet de lancer une interface graphique d’enregistrement de données.

## Déplacer le turtlebot
Les commandes suivantes sont à lancer dans un nouveau terminal du PC après être reliées au turtlebot.

Partez dans votre workspace `catkin_ws/src` et tapez:
```console
git clone  https://github.com/ros-teleop/teleop_twist_keyboard.git
```

Ceci permet de télécharger le paquet concernant la navigation du turtlebot depuis le clavier.

Pour contrôler depuis le clavier, tapez la commande:
```console
cd teleop_twist_keyboard
python3 teleop_twist_keyboard.py
```

Ceci vous présente les commandes suivantes:
```
Reading from the keyboard  and Publishing to Twist!
---------------------------
Moving around:
   u    i    o
   j    k    l
   m    ,    .

For Holonomic mode (strafing), hold down the shift key:
---------------------------
   U    I    O
   J    K    L
   M    <    >

t : up (+z)
b : down (-z)

anything else : stop

q/z : increase/decrease max speeds by 10%
w/x : increase/decrease only linear speed by 10%
e/c : increase/decrease only angular speed by 10%

CTRL-C to quit
```

Où

- i : Avance en ligne droite
- , : Recule en ligne droite
- l : Tourne à droite
- j : Tourne à gauche
- o : Rotation vers la droite
- u : Rotation vers la gauche
- m : Rotation arrière gauche
- . : Rotation arrière droite
- k : Freinage sec
- q/z : Augmente/diminue la vitesse maximale par 10%
- w/x : Augmente/diminue la vitesse linéaire par 10%
- e/c : Augmente/diminue la vitesse angulaire par 10%
- space key, k : Arrêt sec
- Autre touche : Arrêt progressif

> :warning:
> Pour que le contrôle fonctionne correctement, il faut que le terminal, avec lequel a été déclenché le `teleop`, soit actif (sélectionné).

## Création d’une carte de grille d'occupation

Dans le terminal de la connexion ssh au turtlebot, lancez le robot.

Sur le PC, clonez et compilez le paquet du turtlebot dans le ~/catkin_ws/src :
```console
git clone https://github.com/ROBOTIS-GIT/turtlebot3
cd ..
rosdep install --from-paths src --ignore-src -r -y
catkin_make
```

Puis définissez le type du robot:
```console
export TURTLEBOT3_MODEL=TYPE
# TYPE = 'burger' pour le robot burger
# TYPE = 'waffle' pour le robot waffle
```

Lancez sur le PC le noeud de slam:
```console
roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
```

Ensuite, lancez le noeud de teleop du turtlebot dans l'environnement:
```console
cd teleop_twist_keyboard
python teleop_twist_keyboard.py
```

En utilisant cet outil, faites naviguer le robot dans tout l'environnement, puis pour enregistrer la map, tapez:
```console
rosrun map_server map_saver -f ~/map
```

Deux fichiers apparaissent: `map.yaml` et `map.pgm`.

La carte utilise la carte de grille d'occupation bidimensionnelle (OGM), qui est couramment utilisée dans ROS. La carte sauvegardée ressemblera à la figure ci-dessous, où la zone blanche est la zone sans collision, la zone noire est la zone occupée et inaccessible, et la zone grise représente la zone inconnue. Cette carte est utilisée pour la navigation.

## Navigation du robot dans la map

Dans le terminal de la connexion ssh au turtlebot, lancez le robot.

Dans le terminal du PC (Master), exportez le type du robot :
```console
export TURTLEBOT3_MODEL=$TYPE
# $TYPE = 'burger' pour le robot burger
# $TYPE = 'waffle' pour le robot waffle
```

Puis le neoud de navigation :
```console
roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=<path_vers_la_map_enregistrée>/map.yaml
```
Avec `map.yaml` est la map générée dans l’étape précédente.

Une fenêtre RVIZ s’ouvre.

RVIZ est une interface graphique ROS qui vous permet de visualiser un grand nombre d'informations, en utilisant des plugins pour de nombreux types de topics disponibles.

Sur cette fenêtre, sélectionnez `2D Pose Estimation`, puis cliquez sur la carte où se trouve le robot réel et faites glisser la grande flèche verte vers la direction à laquelle le robot fait face.

<img src="2d_pose.png" alt="2d_pose" width="75%" height="75%" title="2d_pose">

Une fois le Pose est bien définie, sélectionnez sur RVIZ `2D Nav Goal`.

<img src="2d_nav.png" alt="2d_nav" width="75%" height="75%" title="2d_nav">

Cliquez sur la carte pour définir la destination du robot et faites glisser la flèche verte vers la direction à laquelle le robot fera face.

<img src="nav.png" alt="2d_nav" width="75%" height="75%" title="2d_nav">

Le robot fait la plannification de la trajectoire pour arriver à la position définie par la commande 2D Nav Goal.

## Simulation Turtlebot

Le robot peut être étudié en simulation, permettant l'ajout d'autres capteurs afin de tester leur comportement.

La simulation utilise Gazebo: un logiciel de simulation pour robots qui offre la possibilité de créer des environnements complexes et réalistes. Avec Gazebo, on peut simuler des robots dans des mondes 3D, en testant leurs capteurs, leurs algorithmes de navigation et de prise de décision sans le risque d'endommager du matériel réel. 

Robotics fournit une simulation du robot turtlebot, qui nécessite l'installation des paquets de message et de simulation.

Dans votre espace de travail `catkin_ws/src`, cloner les paquets suivants :
```console
cd ~/catkin_ws/src
git clone https://github.com/ROBOTIS-GIT/turtlebot3 --branch=noetic-devel
git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs --branch=noetic-devel
git clone https://github.com/ROBOTIS-GIT/turtlebot3_simulations --branch=noetic-devel
``` 

Puis installer les dépendances :
```console
cd ..
rosdep install --from-paths src --ignore-src -r -y
```

Puis compilez l'espace de travail:
```console
catkin_make
```

Enfin sourcez l'espace de travail
```console
source devel/setup.bash
```


Maintenant, le paquet est prêt à être utilisé. 

Exportez le type du robot :
```console
export TURTLEBOT3_MODEL=$TYPE
# $TYPE = 'burger' pour le robot burger
# $TYPE = 'waffle' pour le robot waffle
```

Lancez la simulation du turtlebot dans l'environnement `turtlebot_world`:
```console
roslaunch turtlebot3_simulations turtlebot3_world.launch
```

La fenêtre Gazebo s'ouvre et montre le robot :
<img src="simulation_tb3.png" alt="tb3" width="75%" height="75%" title="tb3">

Vous trouverez les topics comme dans le robot réel en simulation, en tapant la commande :
```console
rostopic list
```

Pour faire la navigation du robot dans l'environnement simulé, lancez sur un nouveau terminal le noeud de SLAM comme réalisé avec le robot réel :
```console
export TURTLEBOT3_MODEL='burger'
source ~/catkin_ws/devel/setup.bash
roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping
```

Puis lancez le noeud de `teleop` puis naviguez dans l'environnement.
<img src="virtual_slam.png" alt="slam" width="75%" height="75%" title="slam">

Enregisrez la map en executant :
```console
rosrun map_server map_saver -f ~/map
```

Pour faire la navigation, lancez le noeud de navigation sur un nouveau terminal : 
```console
export TURTLEBOT3_MODEL='burger'
roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml
```

Et refaites les étapes précédentes :

Sélectionnez `2D Pose Estimation`, puis cliquez sur la carte où se trouve le robot réel et faites glisser la grande flèche verte vers la direction à laquelle le robot fait face.

<img src="2d_pose.png" alt="2d_pose" width="75%" height="75%" title="2d_pose">

Une fois le Pose est bien définie, sélectionnez sur RVIZ `2D Nav Goal`.

<img src="2d_nav.png" alt="2d_nav" width="75%" height="75%" title="2d_nav">

Cliquez sur la carte pour définir la destination du robot et faites glisser la flèche verte vers la direction à laquelle le robot fera face.

<img src="nav.png" alt="2d_nav" width="75%" height="75%" title="2d_nav">

Le robot fait la plannification de la trajectoire pour arriver à la position définie par la commande 2D Nav Goal.


Pour plus d’information et tutoriels, visitez le site web de <a href="https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/">emanual.robotics</a>
