# Programmation ROS 
### Document produit sur la base de <a href="https://wiki.ros.org/ROS/Tutorials">"wiki.ros.org/ROS/Tutorials"</a>.
## Le catkin workspace 

Les packages catkin peuvent être construits selon un projet autonome, de la même façon que les cmake. Catkin fournit également le concept d’espaces de travail, dans lequel vous pouvez construire plusieurs packages interdépendants.

Un catkin workspace est un répertoire (dossier) dans lequel vous pouvez modifier, construire et installer les packages catkin.

L’agencement d’un catkin workspace est typiquement le suivant :
```
workspace_folder/ -- WORKSPACE
 src/ -- SOURCE SPACE
 CMakeLists.txt -- The 'toplevel' CMake file
 package_1/
 CMakeLists.txt
 package.xml
 ...
 package_n/
 CMakeLists.txt
 package.xml
 ...
 build/ -- BUILD SPACE
 CATKIN_IGNORE -- Keeps catkin from walking this directory
 devel/ -- DEVELOPMENT SPACE (set by CATKIN_DEVEL_PREFIX)
 bin/
 etc/
 include/
 lib/
 share/
 .catkin
 env.bash
 setup.bash
 setup.sh
 ...
 install/ -- INSTALL SPACE (set by CMAKE_INSTALL_PREFIX)
 bin/
 etc/
 include/
 lib/
 share/
 .catkin
 env.bash
 setup.bash
 setup.sh
 ...
```

Un catkin workspace peut contenir jusqu’à 4 espaces différents qui ont chacun un rôle bien précis dans le processus de développement de logiciels : 
- L’espace source : il contient le code source des packages catkin. La racine de l’espace source contient un lien symbolique vers le fichier CMakeLists.txt de niveau supérieur. Ce fichier est appelé par CMake lors de la configuration des projets catkin dans l’espace de travail. Il peut être créé en appelant catkin_init_workspace dans le répertoire de l'espace source.

- L’espace Build : CMake et catkin gardent leurs informations `caches` et les fichiers intermédiaires dans cet espace.
- L’espace développement (Devel) : c’est dans cet espace que sont placées les cibles. 
- L’espace Install : une fois les cibles construites, elles peuvent être installées dans l’espace Install en appelant la cible Install, notamment avec `make install`.
- L’espace Result : utilisé pour des résultats génériques...

## Création d’un catkin workspace 
```console
mkdir -p ~/catkin_ws_groupe1/src
cd ~/catkin_ws_groupe1/src
catkin_init_workspace 
```
Qui crée un lien `CMakeLists.txt` -> `/opt/ros/noetic/share/catkin/cmake/toplevel.cmake` 

Si vous exécutez les commandes suivantes : 

```console
cd ~/catkin_ws_groupe1/
ls src 
```

vous devriez obtenir : 

```
CMakeLists.txt 
```

## Construction de l’espace de travail 
Exécutez les commandes suivantes : 

```console
cd ~/catkin_ws_groupe1/
catkin_make 
```

La commande catkin_make est un outil qui facilite le travail avec catkin workspaces. Les répertoires `build` et `devel` ont été créés automatiquement.

Tapez la commande `ll`, vous devriez avoir:

```
build
devel
src 
```

Le répertoire `build` est l’emplacement par défaut de l’espace de construction `build space` et c’est dans ce répertoire que `cmake` et `make` sont appelés à configurer et construire vos packages. Le répertoire `devel` est l’emplacement par défaut de l’espace de développement `devel space`, qui est l’endroit où vos exécutables et les bibliothèques de vos packages vont être installés. Dans le répertoire `devel`, vous pouvez vous apercevoir que plusieurs fichiers `setup.*sh` ont été créés. 

Avant de continuer, `sourcez` votre nouveau fichier setup.*sh : 
```console
source devel/setup.bash
```

## Navigation dans le système de fichiers ROS 

Nous introduisons les concepts du système de fichiers ROS, et l’utilisation des commandes `roscd`, `rosls` et `rospack`. 

Définitions :
- Le package est l’unité de base du code ROS. Chaque package peut contenir des bibliothèques, des exécutables, des scripts ou d'autres objets.
- Un manifeste (package.xml) est une description d'un package. Il sert à définir les dépendances entre packages et à capturer des méta-informations sur le package comme la version, le gestionnaire, la licence, etc...

Le code est réparti entre de nombreux packages ROS. La navigation à l'aide des outils en ligne de commande comme `ls` et `cd` peut être très fastidieux et c'est pourquoi ROS fournit des outils d’aide. 

<strong>Utilisation de rospack (ros+pack(age)) :</strong> rospack permet d'obtenir des informations sur les packages.

Exemple : 

```console
rospack find roscpp 
```

Doit renvoyer :
```
LE_CHEMIN_DE_VOTRE_INSTALLATION/share/roscpp 
```

Ici : `/opt/ros/noetic/share/roscpp` 

<strong>Utilisation de roscd (ros+cd) :</strong> roscd fait partie de la suite rosbash. Il permet de changer de répertoire (cd) directement à un package ou à une stack.

Exemple : 
```
roscd roscpp 
```

Avec la commande `pwd`, on obtient : `/opt/ros/noetic/share/roscpp` 


Le ROS_PACKAGE_PATH doit contenir une liste de répertoires dans lesquels les packages ROS ont été installés : 
```
echo $ROS_PACKAGE_PATH
```

Doit renvoyer `/home/turtlebot/catkin_ws_groupe1/src:/opt/ros/hydro/share:/opt/ros/hydro/stacks`

<strong>Utilisation de roscd log :</strong> il vous emmènera dans le répertoire dans lequel ROS stocke les fichiers log.

Notez que si vous n'avez pas encore exécuté de programmes ROS, cela va donner un message d'erreur indiquant qu'il n'existe pas encore : No active roscore (et le prompt est : ~/.ros/log)

<strong>Utilisation de rosls (ros+ls) :</strong> cette commande fait partie de la suite rosbash. Elle vous permet d'exécuter `ls` directement dans un package par son nom plutôt que par un chemin absolu.

Exemple (n’hésitez pas à utiliser la touche `tabulation`) : 

```console
rosls roscpp_tutorials 
```

Devrait retourner : 

```
cmake package.xml srv
```

## Création d’un package ROS

Qu'est-ce qu’un paquet catkin?

Pour qu’un paquet puisse être considéré comme un package catkin, il doit répondre à quelques exigences :
* Le package doit contenir un fichier package.xml conformes à catkin.
* Le fichier package.xml fournit les meta-informations sur le package.
* Le package doit contenir un fichier CMakeLists.txt.

Il ne peut y avoir plus d’un paquet dans un répertoire. Cela signifie qu’il n’y a pas de packages imbriqués ni plusieurs packages partageant le même répertoire. Le package le plus simple possible pourrait ressembler à ceci : 

```
my_package/
CMakeLists.txt
package.xml
```

Un espace de travail trivial pourrait ressembler à ceci : 
```
workspace_folder/ -- WORKSPACE
src/ -- SOURCE SPACE
CMakeLists.txt -- 'Toplevel' CMake file, provided by catkin
package_1/
CMakeLists.txt -- CMakeLists.txt file for package_1
package.xml -- Package manifest for package_1
...
package_n/
CMakeLists.txt -- CMakeLists.txt file for package_n
package.xml -- Package manifest for package_n 
```

<strong>Création d’un package catkin :</strong> utilisation du script catkin_create_pkg afin de créer un nouveau package pour catkin. 

```console
# Vous devriez l'avoir créé dans le didacticiel sur la création d'un espace de travail.
cd ~/catkin_ws_groupe1/src 
```

Utilisons le script catkin_create_pkg afin de créer un nouveau package appelé `beginner_tutorials` qui dépendra de `std_msgs`, `roscpp` et `rospy` : 

```console
catkin_create_pkg beginner_tutorials std_msgs rospy roscpp 
```

Cela va créer un répertoire `beginner_tutorials` qui contient un `package.xml` et un `CMakeLists.txt`, qui ont été partiellement remplis avec les informations que vous avez données à `catkin_create_pkg`. catkin_create_pkg exige que vous lui donniez un `package_name` et éventuellement une liste de dépendances (de 1er ordre) dont ce package dépend : 

```console
# catkin_create_pkg <package_name> [depend1] [depend2] [depend3] 
```

Entrez dans le répertoire `beginner_tutorials` (cd beginner_tutorials), puis tapez la commande `ll`. Vous devez voir apparaître les fichiers CMakeLists.txt, package.xml, et les répertoires `include` et `src`. Revenez au répertoire `src` en tapant la commande `cd ..`. 

<strong>Dépendances du package :</strong>

Dépendances de 1er ordre : Tapez la commande suivante : 
```console
rospack depends1 beginner_tutorials 
```

Vous devriez obtenir : 
```
std_msgs
rospy
roscpp 
```

Comme vous pouvez le voir, rospack énumère les mêmes dépendances qui ont été utilisées comme arguments lors de l'exécution catkin_create_pkg. Les dépendances d'un package sont stockées dans le fichier package.xml : 

```console
roscd beginner_tutorials
cat package.xml 
```

Vous devriez obtenir : 

```xml
<package>
...
<buildtool_depend>catkin</buildtool_depend>
<build_depend>roscpp</build_depend>
<build_depend>rospy</build_depend>
<build_depend>std_msgs</build_depend>
...
</package> 
```

Dépendances indirectes : Dans de nombreux cas, une dépendance aura aussi ses propres dépendances. Par exemple, Rospy a d'autres dépendances : 
```console
rospack depends1 rospy 
```

Vous devriez obtenir : 
```
Genpy
roscpp
rosgraph
rosgraph_msgs
roslib
std_msgs 
```

Un package peut avoir pas mal de dépendances indirectes. Heureusement, rospack peut déterminer de façon récursive toutes les dépendances imbriquées : 

```console
rospack depends beginner_tutorials 
```

Vous devriez obtenir : 
```
cpp_common
rostime
roscpp_traits
roscpp_serialization
genmsg
genpy
message_runtime
rosconsole
std_msgs
rosgraph_msgs
xmlrpcpp
roscpp
rosgraph
catkin
rospack
roslib
rospy 
```

<strong>Personnalisation du package : </strong>
Personnalisation du package.xml :

- Balise de description : <description>The beginner_tutorials package</description>
- Balises du responsable : <maintainer email="you@yourdomain.tld">Your
Name</maintainer>
- Balise licence : <license>BSD</license>
- Balises de dépendances : 4 catégories
* build_depend (dépendances pour la construction)
* buildtool_depend (dépendances pour les outils de construction)
* run_depend (dépendances pour l'exécution)
* test_depend (dépendances pour le test)

Le package.xml final devrait ressembler à ceci :

```xml
<?xml version="1.0"?>
<package>
<name>beginner_tutorials</name>
<version>0.1.0</version>
<description>The beginner_tutorials package</description>
<maintainer email="you@yourdomain.tld">Your Name</maintainer>
<license>BSD</license>
<url type="website">http://wiki.ros.org/beginner_tutorials</url>
<author email="you@yourdomain.tld">Jane Doe</author>
<buildtool_depend>catkin</buildtool_depend>
<build_depend>roscpp</build_depend>
<build_depend>rospy</build_depend>
<build_depend>std_msgs</build_depend>
<run_depend>roscpp</run_depend>
<run_depend>rospy</run_depend>
<run_depend>std_msgs</run_depend>
</package> 
```

## Ecriture d’un nœud simple `Publisher` / `Subscriber` en C++ 

Un nœud est le terme ROS pour définir un exécutable qui est connecté au réseau ROS. Nous allons créer un nœud publisher (`talker`) qui envoie continuellement un message.

Pour cela, créez un répertoire dans beginner_tutorials : 
```console
mkdir -p ~/catkin_ws_groupe1/src/beginner_tutorials/src
cd ~/catkin_ws_groupe1/src/beginner_tutorials/src 
```

Créez le fichier `talker.cpp` : 
```cpp
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
int main(int argc, char **argv)
{
   ros::init(argc, argv, "talker");
   ros::NodeHandle n;
   ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
   ros::Rate loop_rate(10);
   int count = 0;
   while (ros::ok())
   {
     std_msgs::String msg;
     std::stringstream ss;
     ss << "hello world " << count;
     msg.data = ss.str();
     ROS_INFO("%s", msg.data.c_str());
     chatter_pub.publish(msg);
     ros::spinOnce();
     loop_rate.sleep();
     ++count;
   }
   return 0;
} 
```

Puis créez le fichier `listener.cpp` : 
```cpp
#include "ros/ros.h"
#include "std_msgs/String.h"
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
 ROS_INFO("I heard: [%s]", msg->data.c_str());
}
int main(int argc, char **argv)
{
 ros::init(argc, argv, "listener");
 ros::NodeHandle n;
 ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);
 ros::spin();
 return 0;
} 
```

Vous devez alors ajouter les lignes suivantes à la fin du fichier CMakeLists.txt du répertoire beginner_tutorials : 
```make
## Build talker and listener
include_directories(include ${catkin_INCLUDE_DIRS})
add_executable(talker src/talker.cpp)
target_link_libraries(talker ${catkin_LIBRARIES})
add_dependencies(talker beginner_tutorials_generate_messages_cpp)
add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})
add_dependencies(listener beginner_tutorials_generate_messages_cpp)
```

Cela permettra de créer 2 exécutables, talker et listener, qui seront par défaut dans le répertoire de votre devel space : `~/catkin_ws_groupe1/devel/lib/beginner_tutorials/<package name>` (vérifiez-le).

Lancez maintenant `catkin_make` dans votre catkin space :
```console
# In your catkin workspace
$ catkin_make 
```

<strong>Exécution des programmes : </strong>
Lancez roscore : 
```console
roscore
```

Dans un autre terminal:
```console
# In your catkin workspace
cd ~/catkin_ws_groupe1
source ./devel/setup.bash
```

Puis lancez listener : 

```console
rosrun beginner_tutorials listener
```

Vous constatez que rien ne se passe, en réalité, listener écoute...


Dans un autre terminal : 
```console
# In your catkin workspace
cd ~/catkin_ws_groupe1
source ./devel/setup.bash 
```

Puis lancez talker : 
```console
rosrun beginner_tutorials talker 
```
Ouah... Ca fonctionne... Vous devez obtenir les résultats suivants : 

Terminal1:
```
[INFO] [WallTime: 1314931969.258941]
/listener_17657_1314931968795I heard hello world 1314931969.26
[INFO] [WallTime: 1314931970.262246]
/listener_17657_1314931968795I heard hello world 1314931970.26
[INFO] [WallTime: 1314931971.266348]
/listener_17657_1314931968795I heard hello world 1314931971.26
[INFO] [WallTime: 1314931972.270429]
/listener_17657_1314931968795I heard hello world 1314931972.27
[INFO] [WallTime: 1314931973.274382]
/listener_17657_1314931968795I heard hello world 1314931973.27
[INFO] [WallTime: 1314931974.277694]
/listener_17657_1314931968795I heard hello world 1314931974.28
[INFO] [WallTime: 1314931975.283708]
/listener_17657_1314931968795I heard hello world 1314931975.28
```

Terminal2:
```
[INFO] [WallTime: 1314931831.774057] hello world 1314931831.77
[INFO] [WallTime: 1314931832.775497] hello world 1314931832.77
[INFO] [WallTime: 1314931833.778937] hello world 1314931833.78
[INFO] [WallTime: 1314931834.782059] hello world 1314931834.78
[INFO] [WallTime: 1314931835.784853] hello world 1314931835.78
[INFO] [WallTime: 1314931836.788106] hello world 1314931836.79
```

<strong>Arborescence du disque dur et fichiers / commandes importants : </strong>

<img src="Arborescence.png" alt="Arborescence" width="75%" height="75%" title="Arborescence">