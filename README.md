# TP_ROS1

Ce répertoire contient les énoncés des TP de ROS1 pour l'IUT GEII. Les TP sont répartis de la façon suivante : 
1. TP1 dans le répertoire `ros1_TP1`, est concerné par la prise en main de la plateforme et la compréhension des types de communication:
- Squelette d'un espace de travail (workspace).
- Exemple de code C++ d'un noeud `Publisher/Subscriber` et `Client/Service`.

2. TP2 dans le répertoire `tb3_TP2` aborde l'usage d'un robot réel de type Turtlebot 3 `burger/waffle`:
- communication Master/Slave
- téléopération du robot
- création d'une map et navigation du robot dans ce map.